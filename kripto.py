letters = "abcdefghijklmnopqrstuvwxyz "
invLetters = {}
numLetters = len(letters)

for i in range(numLetters):
    invLetters[letters[i]] = i

def encrypt(plain,password,cols):
    global letters, invLetters, numLetters
    cipher = ""
    passwordIndex = 0
    for i in range(len(plain)):
        shift = invLetters[password[passwordIndex]]
        index = (invLetters[plain[i]] + shift) % numLetters
        cipher = cipher + letters[index]
        passwordIndex = (passwordIndex + 1) % len(password)

    numLines = len(cipher) // cols
    if(numLines * cols) < len (cipher):
        numLines + 1

    block = [[" " for i in range(cols)] for j in range(numLines)]
    
    i = 0
    j = 0
    cipher2 = ""

    for k in range(len(cipher)):
        block[i][j] = cipher[k]
        j = (j+1) % cols
        if j ==0:
            i = i+1

    for j in range(cols):
        for i in range(numLines):
            cipher2 = cipher2+block[i][j]

    return cipher2

def decrypt(cipher,password):
    global letters, invLetters, numLetters
    plain1 = ""
    passwordIndex = 0
    for i in range(len(cipher)):
        shift = invLetters[password[passwordIndex]]
        index = (invLetters[cipher[i]] - shift) % numLetters
        plain1 = plain1 + letters[index]
        passwordIndex = (passwordIndex + 1) % len(password)
    
    numLines = len(plain1) // cols
    if(numLines * cols) < len (cipher):
        numLines + 1

    block = [[" " for i in range(numLines)] for j in range(cols)]

    i = 0
    j = 0

    plain = ""
    for k in range(len(cipher)):
        block[j][i] = cipher[k]
        i = (i+1) % numLines
        if i ==0:
            j = j+1

    for i in range(numLines):
        for j in range(cols):
            plain = plain+block[j][i]

    return plain



plain = input("Plain text: ")
password = input("Password: ")
cols = input("Cols: ")
cols = int(cols)

cipher = encrypt(plain,password, cols)
print("Cipher: " + cipher)
plain = decrypt(cipher,password,cols)
print("Plain: "+plain)
